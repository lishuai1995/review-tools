# -*- coding: utf-8 -*-

import asyncio
import pymongo

setting = {
    "replset": "rs0",
    "nodes": ["172.16.0.225:38017",
              "172.16.0.224:38017"],
    "dbname": "mercury",
    "username": "mercury",
    "password": "MercurY4dm1n!",
}

async def _connect_mongdb():

    client = pymongo.MongoClient(
        host=setting["nodes"],
        replicaSet=setting.get("replset"),
        connect=False,
        username=setting.get("username"),
        password=setting.get("password"),
        authSource=setting.get("dbname")
    )
    mgdb = client.get_database(
        setting['dbname'],
        read_preference=pymongo.ReadPreference.SECONDARY_PREFERRED)
    return mgdb

loop = asyncio.get_event_loop()
mgdb = loop.run_until_complete(_connect_mongdb())

