# -*- coding: utf-8 -*-

import os
import sys

sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

from sanic import Sanic

from src.config import CONFIG
from src.views import html_bp

sys.path.append('../')

app = Sanic(__name__)
app.blueprint(html_bp)
app.config.from_object(CONFIG)


if __name__ == "__main__":

    app.run(host="0.0.0.0", port=9090, debug=False)
