#!/usr/bin/env python

import os
import json as base_json


from sanic import Blueprint
from sanic.response import html, json, text
from jinja2 import Environment, PackageLoader, select_autoescape

from src.Model import Tips
from src.config import CONFIG


html_bp = Blueprint('get_html', url_prefix='review')
html_bp.static('/statics', CONFIG.BASE_DIR + '/statics')


env = Environment(
    loader=PackageLoader('views.get_html', '../templates'),
    autoescape=select_autoescape(['html', 'xml', 'tpl']),
    enable_async=True)


async def template(tpl, **kwargs):
    template = env.get_template(tpl)
    rendered_template = await template.render_async(**kwargs)
    return html(rendered_template)


@html_bp.get("/index")
async def review(request):
    return await template('review.html')

"""
# 用户上传文件存入数据库处理
@html_bp.post("/upload")
async def upload(request):

    # 解析上传的文件，将上传的文件保存至某个路径下
    file_dir = os.path.join(CONFIG.BASE_DIR, 'upload')
    if not os.path.exists(file_dir):
        # TODO：若不存在，创建时需要权限
        os.makedirs(file_dir)

    info = request.files.get("file")
    file_path = file_dir + '/' + info.name
    with open(file_path, 'wb+') as f:
        f.write(info.body)

    with open(file_path, 'r', encoding="utf-8") as ff:
        for line in ff:
            try:
                line = line.rstrip()
                print(line)
                json_data = base_json.loads(line)
                tips = Tips(**json_data)
                await Tips.save(tips)
            except:
                continue

    # 返回的数据必须是  json 格式的 ==》 第三方插件
    data = {
        "code": 0,
        "msg": "",
        "data": {
            "status": "ok"
        }
    }
    return json(data)
"""

# 按照 city_info 查询符合该城市下的城市信息
@html_bp.get("/querydata")
async def querydata(request):
    city_info = request.args.get("city_info")
    data_count = request.args.get("data_count")

    site_level = " ".join(city_info.split("/"))

    tips_list = await Tips.get_tips(site_level, int(data_count))
    data_json = {}
    for i in range(len(tips_list)):
        del tips_list[i]['_id']
        data_json[i] = tips_list[i]
    info_str = "{}".format(data_json)
    info_str = info_str.replace("None", "''")
    info_str = info_str.replace("display: none", " ")
    return text(info_str)


# 按照 city_info 查询符合该城市下的城市信息
@html_bp.get("/queryurl")
async def querydata(request):
    detail_url = request.args.get("detail_url")
    tips = await Tips.get_by_reference(detail_url)
    if tips:
        tips.pop("_id")
    info_str = "{}".format(tips)
    info_str = info_str.replace("None", "111")
    return text(info_str)


# 查询都有哪些省市的tips
@html_bp.get("/queryarea")
async def queryarea(request):
    area_list = await Tips.get_area()
    data = set()
    for info in area_list:
        area = info.split()
        data.update(area)
    data = list(data)
    area = {
        "area": data
    }
    return json(area)


if __name__ == '__main__':

    str_test = "{0: {'cate_xz': '其他类权力', 'cate_parent': '', 'title': '二手车交易市场核准性备案', 'base_info': {'name': '基本信息', 'content': [{'name': '职权名称', 'content': '二手车交易市场核准性备案'}, {'name': '行使主体', 'content': '阳泉市商务粮食局'}, {'name': '职权编码', 'content': '1700-Z-00800-140300'}, {'name': '备注', 'content': None}]}, 'work_flow': {'name': '办理流程', 'content': '', 'picture': ''}, 'materials': {'name': '申请材料', 'content': [{'name': '设立申请', 'content': None}, {'name': '设立可研报告', 'content': None}, {'name': '土地产权证明或租赁协议', 'content': None}, {'name': '内部规章制度', 'content': None}, {'name': '企业名称预先核准通知书', 'content': None}, {'name': '身份证', 'content': None}]}, 'results': {'name': '结果名称及样本', 'content': ''}, 'basis': {'name': '设定依据', 'content': [{'name': '《二手车流通管理办法》（商务部、公安部、工商总局、税务总局2005年第2号令）', 'content': '设立二手车交易市场、二手车经销企业开设店铺，应当符合所在地城市发展及城市商业发展有关规定。第33条：建立二手车交易市场经营者和二手车经营主体备案制度。凡经工商行政管理部门依法登记, 取得营业执照的二手车交易市场经营者和二手车经营主体，应当自取得营业执照之日起2个月内向省级商务主管部门备案。省级商务主管部门应当将二手车交易市场经营者和二手车经营主体有关备案情况定期报送国务院商务主管部门。'}]}, 'fees': {'name': '收费标准及依据', 'content': ''}, 'visits': {'name': '到现场次数', 'content': ''}, 'q&a': {'name': '常见问题', 'content': ''}, 'reference': 'http://yq.sxzwfw.gov.cn/icity/icity/listintquery/qlguide?i=5&code=1700-Z-00800-140300', 'extras': [{'name': '责任事项', 'content': []}, {'name': '责任事项依据', 'content': []}], 'site_level': '山西省 阳泉市'}}"
    print(type(str_test))
    str_test = str_test.replace("None", "''")
    print(str_test)