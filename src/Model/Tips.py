# -*- coding: utf-8 -*-

from bson import ObjectId
from pymongo.errors import DuplicateKeyError

from src.libs.mongo import mgdb


class Tips:

    _db = mgdb.tips

    def __init__(self, **kwargs):
        self._id = None
        for key, value in kwargs.items():
            self.__dict__.update({key: value})

    def to_mongo(self):
        """返回实例在 MongoDB 中的存储数据结构
        """
        if self._id is None:
            del self._id
        elif isinstance(self._id, str):
            self._id = ObjectId(self._id)
        data = self.__dict__.copy()
        return data

    async def save(self):
        data = self.to_mongo()
        try:
            result = await self._db.insert_one(data)
            self._id = result.inserted_id
        except DuplicateKeyError as e:
            print("[DB]<{0}> already exists".format(data))
        except Exception as e:
            print("[DB] inserted: error: {0}".format(data))
        else:
            print("[DB] inserted: succeed: {0}".format(data))

    @classmethod
    async def get_tips(cls, site_level, count):
        if site_level == "山东":
            site_level = "山东 直属"
        elif site_level == "安徽省":
            site_level = "安徽省 "
        elif site_level == "广西":
            site_level = "广西 "
        elif site_level == "重庆市":
            site_level = "重庆市 "
        elif site_level == "云南省":
            site_level = "云南省 "

        result = cls._db.aggregate([{"$match": {"site_level":site_level}}, {"$sample": {'size':count}}])
        tips_list = []
        for cursor in list(result):
            inst = Tips(**cursor)
            tips_list.append(inst.__dict__)
        return tips_list

    @classmethod
    async def get_area(cls):
        area_list = cls._db.distinct( "site_level" )
        return list(area_list)

    @classmethod
    async def get_by_reference(cls, detail_url):
        if not detail_url:
            return None
        data = cls._db.find_one({"reference": detail_url})
        inst = Tips(**data)
        inst = inst.__dict__
        return inst


# 查询该地区有多少数据
# cls._db.aggregate([{"$group": {"_id": "$site_level", "count": {"$sum": 1}}}])
