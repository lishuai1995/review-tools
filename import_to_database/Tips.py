# -*- coding: utf-8 -*-

import json
import os
import sys

from bson import ObjectId
from pymongo.errors import DuplicateKeyError
from import_to_database.mongo import mgdb


sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))


class Tips:

    _db = mgdb.tips

    def __init__(self, **kwargs):
        self._id = None
        for key, value in kwargs.items():
            self.__dict__.update({key: value})

    def to_mongo(self):
        """返回实例在 MongoDB 中的存储数据结构
        """
        if self._id is None:
            del self._id
        elif isinstance(self._id, str):
            self._id = ObjectId(self._id)
        data = self.__dict__.copy()
        return data

    def save(self):
        data = self.to_mongo()
        try:
            result = self._db.insert_one(data)
            self._id = result.inserted_id
        except DuplicateKeyError as e:
            print("[DB]<{0}> already exists", e)
        except Exception as e:
            print("[DB] inserted: error", e)
        else:
            print("[DB] inserted: succeed")


if __name__ == '__main__':

    FILES_PATH = "/data/行政许可/import_data"

    def read_file():
        file_dir = FILES_PATH
        for root, dirs, files in os.walk(file_dir):
            for file in files:
                yield file_dir + "/" + file


    for file_path in read_file():
        count = 1
        with open(file_path, 'r', encoding="utf-8") as ff:
            for line in ff:
                try:
                    line = line.rstrip()
                    json_data = json.loads(line)
                    print(count)
                    tips = Tips(**json_data)
                    count = count + 1
                    Tips.save(tips)
                except:
                    continue
        print(file_path, "count:", count)
