# -*- coding: utf-8 -*-

import json
from math import ceil

import xlwt

def read_count():
    with open("data_count.txt", "r", encoding="utf-8") as ff:
        data_list = []
        for line in ff.readlines():
            line = line.strip()
            data = json.loads(line)
            data_list.append(data)
        return data_list

def calc_sum(data_list):
    sum = 0
    for info in data_list:
       sum = sum + info["count"]
    return sum


def write_to_excel(data_list, sum, special=9000):
    PATH = "tips.xls"
    book = xlwt.Workbook() #创建一个Excel
    sheet1 = book.add_sheet('area') #在其中创建一个名为hello的sheet

    sheet1.write(0, 0, "地区")
    sheet1.write(0, 1, "总量")
    sheet1.write(0, 2, "抽检数量")

    for info in data_list:
        check_count = ceil((info["count"]/sum) * special)
        sheet1.write(data_list.index(info)+1,0,info["_id"])
        sheet1.write(data_list.index(info)+1,1,info["count"])
        sheet1.write(data_list.index(info)+1,2, check_count)
        book.save(PATH)


if __name__=="__main__":

    data_list = read_count()
    sum = calc_sum(data_list)
    print(sum)
    write_to_excel(data_list, sum)

